# NewsLond


[NewsLond](https://luispeixoto.github.io/NewsLond/) é um site de notícias que utiliza o RSS do portal Eurogamer.pt para exibir as postagens.

As notícias são exibidas através da conversão do RSS para JSON, através do site [Rss2json](https://rss2json.com/).

## Preview
[![](https://i.ibb.co/x3GMKHg/preview.png)](https://luispeixoto.github.io/NewsLond/)
